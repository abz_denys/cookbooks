#!/usr/bin/env bash

ansible-playbook --verbose --inventory-file=dev_hosts.ini playbook_initial.yaml
# ansible-playbook --verbose --inventory-file=dev_hosts.ini playbook_ssl_get.yaml
ansible-playbook --verbose --inventory-file=dev_hosts.ini playbook_nginx_conf.yaml
