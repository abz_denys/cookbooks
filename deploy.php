<?php
namespace Deployer;

date_default_timezone_set('UTC');
require 'recipe/common.php';
require 'recipe/laravel.php';
/**
 * ===================================================================================================================
 * Tasks
 * ===================================================================================================================
 */
/**
 * PHP tasks
 */
task(
    'reload:php-fpm',
    function () {
        run('sudo service php7.1-fpm reload');
    }
)->desc('Reload php-fpm');
task(
    'permissions:fix',
    function () {
        run('sudo chown -R www-data.www-data {{deploy_path}}/current/');
        run('sudo chown -R www-data.www-data {{deploy_path}}/shared/');
    }
)->desc('Fix permissions after deployment (set owner to www-data)');
task('update:robots', function () {
    $stage = null;
    if (input()->hasArgument('stage')) {
        $stage = input()->getArgument('stage');
    }
    if ($stage !== 'prod') {
        run("echo \"User-agent: Twitterbot\nAllow: *\nUser-agent: *\nDisallow: /\" > {{release_path}}/public/robots.txt");
    }
})->desc('Prevent bots from indexing dev version of site');
task(
    'supervisor:stop',
    function () {
        run('supervisorctl stop all');
    }
)->desc('Stop supervisor');
task(
    'supervisor:start',
    function () {
        run('supervisorctl start all');
    }
)->desc('supervisorctl start all');
task(
    'npm:install',
    function () {
        $output = run('cd {{release_path}} && npm i');
        writeln('<info>' . $output . '</info>');
    }
)->desc('Install npm packages');
task(
    'frontend:build',
    function () {
        $output = run('cd {{release_path}} && npm run build');
        writeln('<info>' . $output . '</info>');
    }
)->desc('Run webpack');
/**
 * ===================================================================================================================
 * Servers
 * ===================================================================================================================
 */
host('157.230.20.96')
    ->user('root')
    ->port(22)
    ->identityFile(__DIR__ . '/.ssh/id_rsa')
    ->stage('dev')
    ->set('deploy_path', '/var/www/test2018_denis.abz.agency')
    ->set('env', 'dev')
    ->set('branch', 'master');

/**
 * ===================================================================================================================
 * Global deployment configuration
 * ===================================================================================================================
 */
set('repository', 'git@bitbucket.org:abz_denys/test2018_denis.abz.agency.git');
set('ssh_type', 'native');
set('ssh_multiplexing', false);


task('deploy', [
    'deploy:prepare',
    'deploy:release',
    'deploy:update_code',
    'deploy:writable',
    'npm:install',
    'frontend:build',
    'deploy:symlink',
    'permissions:fix',
    'cleanup',
])->desc('Deploy your project');

after('deploy', 'success');

// После деплоя перезапустим php
after('deploy', 'reload:php-fpm');

// После отката на прошлый релиз - тоже перезапустим его
after('rollback', 'reload:php-fpm');
